export const parseUserIds = userIds => userIds.split(/\s*,\s*/g);

export const stringifyUserIds = userIds => userIds.join(',');
