# frozen_string_literal: true

module Analytics
  class GroupValueStreamSerializer < BaseSerializer
    entity ::Analytics::GroupValueStreamEntity
  end
end
